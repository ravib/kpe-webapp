#CS448 - KickstarterProjectEvaluator
 
##Installation/Usage

####With Docker (recommended)

- **Get started with Docker** 

    If you don't have Docker installed, follow the steps [here](https://docs.docker.com/engine/installation/) and install Docker. 
    If docker is already installed, skip this step and move on to the next step.  

- **Run the server locally**
    
    You don't have to checkout this project manually. Just run the following command after installing docker and 
    you should have the server running locally.  
     
    ```
    docker run -d -p 5000:5000 --name cs498-kpe-webapp-img ravishankarb/cs498-kpe-webapp-img:v1.0 
    ```
    
    When you run this the first time, the required images will be downloaded and cached. As a result, it'll be slowish 
    the first time around. Subsequent runs will be relatively faster.
    
- **Verify docker container startup**    
    
    ```
    docker ps -a
    ```
    
    If all went well, you should see something like this:

CONTAINER ID  | IMAGE | COMMAND | CREATED | STATUS | PORTS
--- | --- | --- |--- | --- | --- | 
some_id | ravishankarb/cs498-kpe-webapp-img:v1.0 | .. | .. | .. | 0.0.0.0:5000->5000

- **To check the logs**   

    ```
    docker logs cs498-kpe-webapp-img
    ```
    
- **Open the webapp from browser**

    From a browser of your choice (tested with chrome), goto:
    [http://0.0.0.0:5000/home](http://0.0.0.0:5000/home)

   
###Deployed on Cloud
TODO

###Need usage related help ? 
    
- When the webapp loads, the page will show helpful visual hints that will introduce the user to the GUI components. (a simple navigation walk through). 


##Uninstall

The following commands are one way to quickly stop & remove ALL containers 
(assuming you followed the recommended docker installation steps). Disclaimer: Containers unrelated 
to this project will be impacted by these steps. So if you have multiple containers running, pass the id 
and make these commands specific instead of all inclusive.  

```
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi -f <id>
```

 