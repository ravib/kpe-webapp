
var generateRandomInput = function(){
    //TODO enter a couple of formatted json string here.
    var input_samples = [
        "Sample json 1",
        "Sample json 2"
    ];
    return input_samples[Math.floor(Math.random()*input_samples.length)];
}


var processInput = function(){
    postAsync($('#input-text').val());
}


var processRandomInput = function(){
    var input = generateRandomInput();
    $('#input-text').val(input)
    postAsync(input);
}


var postAsync = function(input){
     console.log("input = ", input);
    $("#loading").toggle();

    $.ajax({
        url : "/process",
        type: "POST",
        data : {
            input: input
        },
        success: function(data, textStatus, jqXHR) {
            $("#loading").toggle();
            $("#result-container").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#loading").toggle();
        }
    });
}

var getAssumptions = function(){

    $.ajax({
        url : "/assumptions",
        type: "GET",
        success: function(data, textStatus, jqXHR) {
            $("#result-container").html(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {

        }
    });
}
