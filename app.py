import os
from flask import Flask, render_template
from flask import request

from service import InputProcessor

app = Flask(__name__)

@app.route('/')
def landing():
    return render_template('home.html')


@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/assumptions')
def assumptions():
    return render_template('assumptions.html')

@app.route('/process', methods=['POST'])
def process():
    input = request.form["input"]
    print("input = ", input)
    processor = InputProcessor()
    result = processor.process(input)

    return render_template('result.html',
                           result=result)


@app.after_request
def apply_caching(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    return response


if __name__ == '__main__':

    extra_dirs = ['./model',
                  './static/css',
                  './static/js',
                  './templates']
    extra_files = extra_dirs[:]

    for extra_dir in extra_dirs:
        for root, dirs, files in os.walk(extra_dir):
            for filename in files:
                filename = os.path.join(root, filename)
                if os.path.isfile(filename):
                    extra_files.append(filename)
    print(extra_files)

    app.run(host='0.0.0.0', extra_files=extra_files, debug=False)
