FROM ubuntu:16.04
FROM python:3.6.3-jessie
MAINTAINER Ravi B

USER root

# Define timezone
ENV TZ=America/Los_Angeles
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install flask numpy

# Install wget
RUN apt-get -y install wget git

WORKDIR /opt

RUN git clone https://ravib@bitbucket.org/ravib/kpe-webapp.git

CMD [ "python", "./kpe-webapp/app.py" ]
EXPOSE 5000